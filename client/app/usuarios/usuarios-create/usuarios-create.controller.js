'use strict';
(function(){

class UsuariosCreateComponent {
  constructor(usuariosService,tiposDocumentosService,ciudadesService, $state,departamentsService) {
    this.usuariosService = usuariosService;
    this.ciudadesService = ciudadesService;
    this.tiposDocumentosService = tiposDocumentosService;
    this.departamentsService=departamentsService;
    this.$state = $state;
    this.showValidaDocumento = false;
  }

  $onInit(){
    this.tiposDocumentosService.query().$promise
    .then(response => {
      console.log('TYPEDOCUMENT OK', response);
      this.tipoDocumento = response;
    })
    .catch(err => {
      console.log('ERROR', err);
    });

    this.ciudadesService.query().$promise
    .then(response => {
      console.log('Ciudades OK', response);
      this.ciudad = response;
      console.log("ciudades",this.ciudad);
    })
    .catch(err => {
      console.log('ERROR', err);
    });

    this.departamentsService.query().$promise
      .then(response => {
        this.departamentsList = response;
        console.log('departamentos ',this.departamentsList);
      })

      .catch(err => {
        console.log(err, 'Error');
      });
  } 

  createUser(){
    this.user.active = true;
    console.log(this.user);
  this.usuariosService.save(this.user).$promise
    .then(response => {
      console.log('CREATE OK',response);
      this.$state.go('usuarios-list');
    })
    .catch(err => {
      console.log('ERROR',err);
    });
  }
 
  getCiudades(){
    console.log(this.idDepartaments);
    this.ciudadesService.getCiudades({idDepartaments:this.idDepartaments}).$promise
    .then(response => {
      console.log('Ciudades', response);
      this.ciudad = response;
    })
    .catch(err => console.error(err));
  }
}

UsuariosCreateComponent.$inject = ['usuariosService','tiposDocumentosService','ciudadesService','$state','departamentsService'];
angular.module('startUpApp')
  .component('usuariosCreate', {
    templateUrl: 'app/usuarios/usuarios-create/usuarios-create.html',
    controller: UsuariosCreateComponent,
    controllerAs:'vm'
  });

})();
