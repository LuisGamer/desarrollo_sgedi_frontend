'use strict';

angular.module("startUpApp")
.factory('AuthService',AuthService);

AuthService.$inject  = ['$auth','$state'];
function AuthService($auth,$state){
	var Auth = {
		login:login,
		isAuthenticated: isAuthenticated,
		logout:logout,
		isAdmin:isAdmin,
		isUser:isUser,
		isEmpl:isEmpl

	};

	function login(user,collback){
		$auth.login(user)
		.then(response => {
			console.log("Acaba de iniciar seccion",response);

			$state.go('profile');
		})
		.catch(err =>{
			console.log("Error de login vuelvalo a intentar",err);
			$state.go('login');
		})
	}

function logout(){
if(Auth.isAuthenticated()){
 $auth.logout()
 .then(response => {
	 $state.go("main");
	 console.log("Gracias por usar nuestros servicios");
 })
 }
}

function isAuthenticated() {
	if ($auth.isAuthenticated()) {
	 return true;
 }else{
	 return false;
 }
}
function isAdmin() {
	if(Auth.isAuthenticated()){
		if($auth.getPayload().roles.indexOf("ADMIN")!== -1){
      return true;
		}else{
			return false;
		}
	}else {
		return false;
	}
}
	function isUser() {
		if(Auth.isAuthenticated()){
			if($auth.getPayload().roles.indexOf("USER")!== -1){
	      return true;
			}else{
				return false;
			}
		}else {
			return false;
		}
}
function isEmpl() {
	if(Auth.isAuthenticated()){
		if($auth.getPayload().roles.indexOf("EMPL")!== -1){
			return true;
		}else{
			return false;
		}
	}else {
		return false;
	}
}
	return Auth;
}
