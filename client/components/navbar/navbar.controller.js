'use strict';

class NavbarController {
  constructor(AuthService){
		  this.AuthService = AuthService;
		  this.isCollapsed = true;
  }
  ocultarCollapse(){
   $('.navbar-collapse').collapse('hide');
                }

}

NavbarController.$inject = ['AuthService']
angular.module('startUpApp')
  .component('navbar', {
  	templateUrl: 'components/navbar/navbar.html',
    controller: NavbarController,
    controllerAs: 'vm'
  });
